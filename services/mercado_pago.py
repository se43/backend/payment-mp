import mercadopago

sdk = mercadopago.SDK(
    "TEST-1475553624116644-072618-a28147bed61da49a5458e2b1a1ba206a-797262186"
)


async def status_order(data):
    print(data)
    data["transaction_amount"] = data["transactionAmount"]
    data.pop("transactionAmount")
    data["issuer_id"] = data["issuerId"]
    data.pop("issuerId")
    data["payment_method_id"] = data["paymentMethodId"]
    data.pop("paymentMethodId")
    print(data)
    payment_response = sdk.payment().create(data)
    print(payment_response)
    return payment_response["response"]
