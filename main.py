from fastapi import FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import ORJSONResponse

from models.mercado_pago import client_payment_data
from services.mercado_pago import status_order

app = FastAPI(
    title="Sem Muvuca",
    description="Python/FastAPI based service for all 'Sem Muvuca' functionality.",
    default_response_class=ORJSONResponse,
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/process_payment", status_code=status.HTTP_201_CREATED)
async def payment(data: client_payment_data):
    return await status_order(data.__dict__)
